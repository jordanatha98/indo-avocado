import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppTemplate } from '@indoavocado/templates/app.template';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                loadChildren: () => import('@indoavocado/home/pages/home.page.module').then(m => m.IndoavocadoHomePageModule),
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
