import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HomePage } from '@indoavocado/home/pages/home.page';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CarouselModule } from 'ngx-owl-carousel-o';

const MODULES: any[] = [
    CommonModule,
    RouterModule,
    IvyCarouselModule,
    CarouselModule,
];

@NgModule({
    declarations: [HomePage],
    imports: [
        ...MODULES,
        RouterModule.forChild([
            {
                path: '',
                component: HomePage
            },
        ]),
        FontAwesomeModule
    ],
})
export class IndoavocadoHomePageModule {}
