import { ChangeDetectionStrategy, Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { ViewportScroller } from '@angular/common';
import { faBox, faChevronDown, faHome, faQuestion, faQuoteRight } from '@fortawesome/free-solid-svg-icons';
import { faTwitter, faFacebook, faWhatsapp } from '@fortawesome/free-brands-svg-icons';

@Component({
    selector: 'indoavocado-home-page',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomePage {
    public faHome = faHome;
    public faQuoteRight = faQuoteRight;
    public faBox = faBox;
    public faQuestion = faQuestion;
    public faChevronDown = faChevronDown;
    public faTwitter = faTwitter;
    public faFacebook = faFacebook;
    public faWhatsapp = faWhatsapp

    @HostListener('window:scroll') onScroll(e: Event): void {
        if (this.intro && this.jumbotron && this.product && this.faq) {
            console.log(window.scrollY, this.faq.nativeElement.offsetTop)
            if (window.scrollY < this.intro.nativeElement.offsetTop - 60) {
                this.activeNav = 'home';
            } else if(window.scrollY < this.product.nativeElement.offsetTop && window.scrollY > this.intro.nativeElement.offsetTop-60) {
                this.activeNav = 'intro';
            } else if(window.scrollY < this.faq.nativeElement.offsetTop - 300 && window.scrollY > this.product.nativeElement.offsetTop) {
                this.activeNav = 'product';
            } else if(window.scrollY > this.faq.nativeElement.offsetTop - 200) {
                this.activeNav = 'faq';
            }
        }
        if (window.scrollY > 100) {
            this.navbarActive = true;
        } else {
            this.navbarActive = false;
        }
    }

    @ViewChild('jumbotron') public jumbotron: ElementRef;
    @ViewChild('intro') public intro: ElementRef;
    @ViewChild('product') public product: ElementRef;
    @ViewChild('faq') public faq: ElementRef;

    @ViewChild('panel1') public panel1: ElementRef;
    @ViewChild('panel2') public panel2: ElementRef;
    @ViewChild('panel3') public panel3: ElementRef;
    @ViewChild('panel4') public panel4: ElementRef;
    public navbarActive: boolean;
    public activeNav: string;
    public activeProduct: string;
    public activeFaq: string;

    public constructor(
        private scroller: ViewportScroller,
    ) {
        this.navbarActive = false;
        this.activeNav = 'home';
        this.activeProduct = 'farm';
        this.activeFaq = null;
    }

    public handleWhatsappClicked(): void {
        window.open('https://wa.me/6287877242041', '_blank')
    }

    public scrollTo(el: HTMLElement) {
        el.scrollIntoView({ behavior: 'smooth' });
    }

    public handleFaqClicked(el: HTMLElement, activePanel: string): void {
        if (el.style.maxHeight === '0px' || !el.style.maxHeight) {
            el.style.maxHeight = el.scrollHeight + 18 + 'px';
            this.activeFaq = activePanel;
        } else {
            el.style.maxHeight = '0px';
            this.activeFaq = null;
        }
    }
}
