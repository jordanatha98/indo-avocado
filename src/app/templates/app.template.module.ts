import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppTemplate } from './app.template';

const MODULES: any[] = [
    CommonModule,
    RouterModule,
];

@NgModule({
    declarations: [AppTemplate],
    imports: [...MODULES],
})
export class AppTemplateModule {}
