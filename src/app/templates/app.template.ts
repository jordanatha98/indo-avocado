import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'indoavocado-app-template',
    templateUrl: './app.template.html',
    styleUrls: ['./app.template.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppTemplate {
    public scroll(el: HTMLElement) {
        el.scrollIntoView({behavior: 'smooth'});
    }
}
