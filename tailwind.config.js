module.exports = {
    prefix: '',
    purge: {
        enabled: process.env.PURGE_TW === 'true',
        content: [
            './src/**/*.{html,ts}',
        ]
    },
    darkMode: 'class', // or 'media' or 'class'
    theme: {
        screens: {
            xs: '420px',
            sm: '640px',
            md: '768px',
            lg: '1024px',
            xl: '1200px',
        },
        extend: {},
        important: true,
    },
    variants: {
        extend: {},
    },
};
